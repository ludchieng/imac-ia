var SIZE = 900 / 200;
var cells = [];
var next = [];
function setup() {
    createCanvas(900, 900);
    noStroke();
    noFill();
    frameRate(10);
    for (var i = 0; i < width / SIZE; ++i) {
        cells.push([]);
        next.push([]);
        for (var j = 0; j < height / SIZE; ++j) {
            cells[i].push({ i: i, j: j, alive: random() > 0.9 });
        }
    }
}
function draw() {
    background(50);
    for (var _i = 0, cells_1 = cells; _i < cells_1.length; _i++) {
        var row = cells_1[_i];
        for (var _a = 0, row_1 = row; _a < row_1.length; _a++) {
            var c = row_1[_a];
            next[c.i][c.j] = { i: c.i, j: c.j, alive: willBeAlive(c) };
        }
    }
    var temp = cells;
    cells = next;
    next = temp;
    for (var _b = 0, cells_2 = cells; _b < cells_2.length; _b++) {
        var row = cells_2[_b];
        for (var _c = 0, row_2 = row; _c < row_2.length; _c++) {
            var c = row_2[_c];
            push();
            if (c.alive)
                fill(color('#fff'));
            translate(c.i * SIZE, c.j * SIZE);
            rect(0, 0, SIZE, SIZE);
            pop();
        }
    }
}
function neighbors(_a) {
    var i = _a.i, j = _a.j;
    var count = 0;
    if (cells[mod(i - 1, width / SIZE)][mod(j + 0, height / SIZE)].alive)
        count++;
    if (cells[mod(i - 1, width / SIZE)][mod(j + 1, height / SIZE)].alive)
        count++;
    if (cells[mod(i + 0, width / SIZE)][mod(j + 1, height / SIZE)].alive)
        count++;
    if (cells[mod(i + 1, width / SIZE)][mod(j + 1, height / SIZE)].alive)
        count++;
    if (cells[mod(i + 1, width / SIZE)][mod(j + 0, height / SIZE)].alive)
        count++;
    if (cells[mod(i + 1, width / SIZE)][mod(j - 1, height / SIZE)].alive)
        count++;
    if (cells[mod(i + 0, width / SIZE)][mod(j - 1, height / SIZE)].alive)
        count++;
    if (cells[mod(i - 1, width / SIZE)][mod(j - 1, height / SIZE)].alive)
        count++;
    return count;
}
function willBeAlive(c) {
    var count = neighbors(c);
    return c.alive
        ? count === 2 || count === 3
        : count === 3;
}
function mod(gerard, n) {
    return ((gerard % n) + n) % n;
}
//# sourceMappingURL=../sketch/sketch/build.js.map