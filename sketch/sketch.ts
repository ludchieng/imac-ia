type Cell = {
  i: number,
  j: number,
  alive: boolean,
}

const SIZE = 900/200;

let cells: Cell[][] = []
let next: Cell[][] = []

function setup() {
  createCanvas(900, 900)
  noStroke()
  noFill()
  frameRate(10)
  for (let i = 0; i < width/SIZE; ++i) {
    cells.push([])
    next.push([])
    for (let j = 0; j < height/SIZE; ++j) {
      cells[i].push({i, j, alive: random() > 0.9})
    }
  }
}

function draw() {
  background(50)

  for (const row of cells) {
    for (const c of row) {
      next[c.i][c.j] = {i: c.i, j: c.j, alive: willBeAlive(c)}
    }
  }

  const temp = cells
  cells = next
  next = temp

  for (const row of cells) {
    for (const c of row) {
      push()

      if (c.alive)
        fill(color('#fff'))
  
      translate(c.i * SIZE, c.j * SIZE)
      rect(0, 0, SIZE, SIZE)
      pop()
    }
  }
}

/**
 * Returns neighbors count around the given cell
 */
function neighbors({i, j}: Cell) {
  let count = 0
  if (cells[ mod(i-1, width/SIZE) ][ mod(j+0, height/SIZE)].alive) count++;
  if (cells[ mod(i-1, width/SIZE) ][ mod(j+1, height/SIZE)].alive) count++;
  if (cells[ mod(i+0, width/SIZE) ][ mod(j+1, height/SIZE)].alive) count++;
  if (cells[ mod(i+1, width/SIZE) ][ mod(j+1, height/SIZE)].alive) count++;
  if (cells[ mod(i+1, width/SIZE) ][ mod(j+0, height/SIZE)].alive) count++;
  if (cells[ mod(i+1, width/SIZE) ][ mod(j-1, height/SIZE)].alive) count++;
  if (cells[ mod(i+0, width/SIZE) ][ mod(j-1, height/SIZE)].alive) count++;
  if (cells[ mod(i-1, width/SIZE) ][ mod(j-1, height/SIZE)].alive) count++;
  return count
}

/**
 * Returns true if the cell should be alive on the next frame
 */
function willBeAlive(c: Cell): boolean {
  const count = neighbors(c);
  return c.alive
    ? count === 2 || count === 3
    : count === 3
}

/**
 * Performs modulus without negative value
 */
function mod(gerard: number, n: number) {
  return ((gerard%n)+n)%n
}
